<!--------------NEWS AND ASIDE------------>
<section class="section_wrapper">
  <div class="news-quicklink-main">
      <div class="container">
          <div class="row">
              <div class="col-lg-9 news-body">
                  <div class="main-heading"><h3><?php echo $details->title; ?></h3></div>
                      <div class="row news-main">
                      <?php
                        if($details)
                        {
                          
                            ?>
                            <div class="col-lg-12">
                                <?php echo $details->description; ?>
                            </div>
                            <?php
                          
                        }
                      ?>
                            
                            
                        </div>
                    </div>
                <?php $this->load->view('common/sidebar');?>
            </div>
        </div>
    </div>
</section>
