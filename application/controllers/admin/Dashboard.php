<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('id'))
        {
            redirect('login');
        }
        $this->load->model('Setting');
        //$this->load->library('Common');
    } 
	public function index()
	{
        $data['title'] = 'Dashboard - '.$this->session->userdata('site_title');
        
        $this->load->model('Page');
        $data['count_pages'] = $this->Page->countAll();

        $this->load->model('Slider');
        $data['count_sliders'] = $this->Slider->countAll();

		$this->load->view('admin/include/header',$data);
        $this->load->view('admin/dashboard/index',$data);
        $this->load->view('admin/include/footer');
	}
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('admin/login');
    }
    public function settings()
    {
        if($this->input->post())
        {
            $arr = $this->input->post();
            if($this->input->post('new_password'))
            {
                $arr['password'] =  sha1($this->input->post('new_password'));  
            }
            $this->Setting->saveData($arr);
            $this->session->set_flashdata('message','Your Setting has been updated successfully!');
            redirect('admin/dashboard/settings');
            
        }
        $data['model'] = $this->Setting->getLoggedInUser();
        $data['title'] = 'Settings - '.$this->session->userdata('site_title');
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/dashboard/settings',$data);
        $this->load->view('admin/include/footer');
    }
    public function theme()
    {
        $this->load->model('DashboardM');
        $model = $this->DashboardM->getSetting();
        if($this->input->post())
        {
            $this->load->library('Common');
            $fields = $this->common->fields('tbl_settings');
            
            if($this->input->post())
            {
                $status = $this->DashboardM->saveSetting($model->id,$fields);
                $this->session->set_flashdata('message','Theme '.$status.' successfully');
                redirect('admin/dashboard/theme');
            }
            
        }
        $data['model'] = $model;
        $data['title'] = 'Theme Setting - SBI';
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/dashboard/theme',$data);
        $this->load->view('admin/include/footer');
    }
    public function upload()
    {
        $this->load->library('Common');
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'])
        {
            $name = $_FILES['file']['name'];
            $arr_files = explode('.',$name);
            $ext = end($arr_files);
            $valid = $this->common->checkValidExt($ext);
            if($valid)
            {
                $arr['file'] = date('YmdHis').'_'.rand(1,999999).'.'.$ext;
                if(move_uploaded_file($_FILES['file']['tmp_name'],APPPATH.'../assets/img/faces/'.$arr['file'])){
                $arr['error'] = false;
                
                }
                else
                {
                    $arr['error'] = 'Could not upload the file';
                    $arr['file'] = false;
                }
            }
            else
            {
                $arr['error'] = 'Invalid File Extension';
                $arr['file'] = false;
                
            }
            
        }
        else
        {
            $arr['error'] = 'Unknown Error';
            $arr['file'] = false;
        }
        echo json_encode($arr);die();
    }
}
