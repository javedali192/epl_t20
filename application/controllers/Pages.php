<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
        $this->load->model('Slider');
        $this->load->model('NewsModel');
        $this->load->model('Page');
        $this->load->helper('text');
    }
    public function index()
    {
        $data['title'] = 'Home - '.$this->session->userdata('site_title');
        $data['sliders'] = $this->Slider->getActiveSlider();
        $data['news'] = $this->NewsModel->getHomeNews();
        $data['aboutus'] = $this->Page->getAboutUs();
        $data['mTime'] = $this->NewsModel;
        $this->load->view('common/header',$data);
        $this->load->view('pages/index',$data);
        $this->load->view('common/footer');
    }
    public function details($slug="")
    {
        $data['title'] = $slug." -".$this->session->userdata('site_title');
        $data['details'] = $this->Page->getPageBySlug($slug);
        $this->load->view('common/header',$data);
        $this->load->view('pages/details',$data);
        $this->load->view('common/footer');
    }
    public function contactUs()
    {
        if($this->input->post())
        {
            $this->sendMail();
        }
        $data['title'] = 'Contact Us - '.$this->session->userdata('site_title');
        $result = $this->db->get('tbl_admin')->row();
        $data['mContactus'] = $result;
        $this->load->view('common/header',$data);
        $this->load->view('pages/contactus',$data);
        $this->load->view('common/footer');
    }

    public function sendMail()
    {
        $admin = $this->db->get('tbl_admin')->row();
        //var_dump($admin);die();
        if($admin && $admin->email)
        {
            $receiver_email = $admin->email;
        }
        else
        {
            $receiver_email = '';
        }
        $name = $this->input->post('name');
        $from_email = $this->input->post('email');
        $contact = $this->input->post('contact');
        $subject = $this->input->post('subject');
        $msg = $this->input->post('message');
        $message = 'Hi there,<br><br>You have received a new contact message from EPL_T20 with following details.
        <p>
        <b>Name: </b>'.$name.'<br>
        <b>Contact: </b>'.$contact.'<br>
        <b>Subject: </b>'.$subject.'<br>
        <b>Message: </b><br>'.$msg.'<br>
        </p><br><br>Regards,<br>Smartcard';
        $to_email = $receiver_email;
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);                        
        $this->email->from('noreply@eplt20.com', 'EPL_T20');
        $this->email->to($to_email);
        $this->email->subject('EPL_T20 - Contact Message');
        $this->email->message($message);
        if ($receiver_email && $this->email->send() && $message)
        {
            $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Your mail has been sent successfully!</div>');
            redirect('pages/contactUs');
        }
        else
        {
            $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">There is error in sending mail! Please try again later</div>');
            redirect('pages/contactUs');
        }
    }

}