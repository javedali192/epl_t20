<script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script src="<?php echo site_url('assets/js/upload.js')?>" type="text/javascript"></script>
<script src="http://jcrop-cdn.tapmodo.com/v0.9.12/js/jquery.Jcrop.min.js"></script>
<link rel="stylesheet" href="http://jcrop-cdn.tapmodo.com/v0.9.12/css/jquery.Jcrop.css" type="text/css" />

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                        <i class="material-icons">build</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">Add/Edit News</h4>
                        
                            <div class="col-md-12">
                                <div class="table-responsive table-sales">
                                        <form action="" method="post" id="settings">

                                                <div class="col-md-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Title</label>
                                                        <input type="text" class="form-control" required="" name="title" value="<?php echo $model->title;?>">
                                                    </div>
                                                </div>                                                            
                                                <div class="clearfix"></div>
                                                
                                                <div class="col-md-12">
                                                                
                                                    <div class="form-group label-floating">
                                                        <label>Description</label>
                                                        <textarea name="description" required=""><?php echo $model->description;?></textarea>
                                                        
                                                    </div>
                                                    
                                                </div>                                                            
                                                <div class="clearfix"></div>

                                                 <div class="col-md-3">
                                                    
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Image</label>
                                                        <a href="javascript:void(0);" id="upload" class="btn btn-info btn-sm">Browse</a>
                                                        <br />
                                                        <a href="javascript:void(0);" id="crop" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#cropModal">Crop</a>
                                                    </div>
                                                    
                                                </div> 
                                                <div class="col-md-9">      
                                                        <input type="hidden" name="image" class="image_name" value="<?php echo $model->image;?>" />                                                          
                                                        <div class="img-holder-news">
                                                            <?php if($model->image){
                                                                ?>
                                                                <img src="<?php echo base_url().'assets/images/news/resized/'.$model->image;?>" style="max-width:100%;" />
                                                                <?php
                                                            }?>
                                                            
                                                        </div> 
                                                            <input type="hidden" name="x" class="x" value="0" />
                                                            <input type="hidden" name="y" class="y" value="0" />
                                                            <input type="hidden" name="w" class="w" value="700" />
                                                            <input type="hidden" name="h" class="h" value="180" />
                                                        <div id="cropModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="cropModalLabel">
                                                            <div class="modal-dialog modal-lg slidermodal" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                        <h4 class="modal-title" id="myModalLabel">Position the  slider</h4>
                                                                    </div>
                                                                    <div class="modal-body" id="toCrop" style="padding: 0!important;margin:20px 0;">
                                                                        <?php if($model->image){?><img src="<?php echo base_url().'assets/images/news/main/'.$model->image;?>" /><?php }?>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                        <button type="button" class="btn btn-primary" id="saveImage">Save Image</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>                                                               
                                                </div>                                                              
                                                <div class="clearfix"></div>

                                                <div class="col-md-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Author</label>
                                                        <input type="text" class="form-control" required="" name="author" value="<?php echo $model->author;?>">
                                                    </div>
                                                </div>                                                            
                                                <div class="clearfix"></div>
                                                
                                                <div class="col-md-12">
                                                    
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Facebook</label>
                                                            <input type="url" class="form-control" required="" name="facebook" value="<?php echo $model->facebook;?>">
                                                        </div>
                                                    
                                                </div>                                                            
                                                <div class="clearfix"></div>

                                                <div class="col-md-12">
                                                    
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Twitter</label>
                                                            <input type="url" class="form-control" required="" name="twitter" value="<?php echo $model->twitter;?>">
                                                        </div>
                                                    
                                                </div>                                                            
                                                <div class="clearfix"></div>
                                                
                                                <div class="gap"></div>
                                                
                                                <div class="col-md-12">                                                                
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Is active?</label>
                                                            <input type="radio" name="is_active" value="1" <?php if($model->is_active){?>checked="checked"<?php }?> /> Yes &nbsp; &nbsp; <input type="radio" name="is_active" value="0" <?php if(!$model->is_active){?>checked="checked"<?php }?> /> No  
                                                        </div>                                                                
                                                </div>                                                            
                                                <div class="clearfix"></div>
                                                
                                                <div class="gap"></div>
                                                
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-rose pull-right"><?php if($model->id){?>Update<?php }else{?>Create<?php }?></button>
                                                </div>
                                                <div class="clearfix"></div>
                                            
                                            
                                        </form>
                                </div>
                            </div>
                            
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.error{color:#CD0909!important;}
</style>
<?php $this->load->view('admin/js_view/news_js.php',array('id'=>$model->id));?>
 <script type="text/javascript">
    $(function(){
                CKEDITOR.replace( 'description' );
            })
</script>           
            