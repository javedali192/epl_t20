<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_Gjdm_0nJk17UVBPoV5Im40uQeguoRAo"></script>
<?php $this->load->view('admin/js_view/gmap.php');?>
<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">build</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Setting</h4>
                                    
                                        <div class="col-md-10">
                                            <div class="table-responsive table-sales">
                                                <h4 class="card-title">Profile Settings</h4>
                                                    <form action="" method="post" id="settings">
                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Full Name</label>
                                                                        <input type="text" class="form-control" required="" name="fullname" value="<?php echo $model->fullname;?>">
                                                                    </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="clearfix"></div>
                                                            
                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Email</label>
                                                                        <input type="text" class="form-control" required="" name="email" value="<?php echo $model->email;?>">
                                                                    </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="clearfix"></div>
                                                            
                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Phone</label>
                                                                        <input type="text" class="form-control" required="" name="phone" value="<?php echo $model->phone;?>">
                                                                    </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="clearfix"></div>
                                                            
                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Username</label>
                                                                        <input type="text" class="form-control" required="" name="username" value="<?php echo $model->username;?>">
                                                                    </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="clearfix"></div>

                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Facebook</label>
                                                                        <input type="url" class="form-control" required="" name="facebook" value="<?php echo $model->facebook;?>">
                                                                    </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="clearfix"></div>

                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Twitter</label>
                                                                        <input type="url" class="form-control" required="" name="twitter" value="<?php echo $model->twitter;?>">
                                                                    </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="clearfix"></div>

                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Linked In</label>
                                                                        <input type="url" class="form-control" required="" name="linkedin" value="<?php echo $model->linkedin;?>">
                                                                    </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="clearfix"></div>

                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Google Plus</label>
                                                                        <input type="url" class="form-control" required="" name="googleplus" value="<?php echo $model->googleplus;?>">
                                                                    </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="clearfix"></div>

                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Youtube</label>
                                                                        <input type="url" class="form-control" required="" name="youtube" value="<?php echo $model->youtube;?>">
                                                                    </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="clearfix"></div>

                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Location</label>
                                                                        <input type="text" class="form-control" id="formattedAddress" class="location" required="" name="location" value="<?php if(!$model->location)echo "Singha Durbar, Kathmandu";else echo $model->location;?>">
                                                                        <input type="hidden" name="latitude" value="<?php if($model->latitude)echo $model->latitude;else echo "27.6982754"?>" id="latitude" onchange='updateMapPinPosition()' />
                                                                        <input type="hidden" name="longitude" value="<?php if($model->longitude)echo $model->longitude;else echo "85.323064"?>" id="longitude" onchange='updateMapPinPosition()' />
                                                                        <div id="map_canvas" style="height: 200px;background:#e5e5e5;margin-top:15px;"></div>
                                                                    </div>
                                                                
                                                            </div>                                                            
                                                            <div class="clearfix"></div>
                                                            
                                                            <div class="gap"></div>
                                                            
                                                            
                                                            
                                                            <div class="col-md-12">
                                                                    <h4>Change Password</h4>
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">New Password</label>
                                                                        <input type="password" class="form-control" id="new_password" name="new_password">
                                                                    </div>
                                                                
                                                            </div>
                                                            
                                                            <div class="clearfix"></div>
                                                            
                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Confirm Password</label>
                                                                        <input type="password" class="form-control" id="confirm_password" name="confirm_password">
                                                                    </div>
                                                                
                                                            </div>
                                                            
                                                            
                                                            
                                                       
                                                        
                                                        
                                                        
                                                        
                                                        <div class="col-md-12">
                                                            <button type="submit" class="btn btn-rose pull-right">Update</button>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        
                                                        
                                                    </form>
                                            </div>
                                        </div>
                                        
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>
            <style>
            .error{color:#CD0909!important;}
            </style>
            <script>
            $(function(){
                $( "#settings" ).validate({
                  rules: {
                    confirm_password: {
                      equalTo: "#new_password"
                    }
                  }
                });

            })
            </script>