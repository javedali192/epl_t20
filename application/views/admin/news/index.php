<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                        <i class="material-icons">code</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">All News</h4>
                        <a href="<?php echo site_url('admin/news/add');?>" class="btn btn-info pull-right"><i class="material-icons">add</i> Add new</a>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive table-sales">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><strong>S.N</strong></th>
                                                <th><strong>Title</strong></th>
                                                <th><strong>Action</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if($news)
                                            {
                                                foreach($news as $k=>$nws)
                                                {
                                                  ?>
                                                  <tr>
                                                    <td>
                                                        <?php
                                                            echo ++$k;
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                            echo $nws->title;
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <a title="Edit News" href="<?php echo site_url('admin/news/add/'.$nws->id);?>" class="btn btn-sm btn-success"><i class="material-icons">edit</i></a>
                                                        <a title="Delete News" href="<?php echo site_url('admin/news/delete/'.$nws->id);?>" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to delete this News?')"><i class="material-icons">delete</i></a>
                                                    </td>
                                                </tr>
                                                  <?php  
                                                }
                                                
                                            }
                                            else
                                            {
                                                ?>
                                                <tr>
                                                    <td colspan="4">
                                                        <p>No news found</p>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <nav aria-label="Page navigation" class="col-md-12 text-center">
                         <ul class="pagination">
                            <?php echo $this->pagination->create_links();?>
                        </ul>
                    </nav>
            </div>
        </div>
    </div>
</div>
</div>