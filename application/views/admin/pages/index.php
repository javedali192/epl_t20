<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon" data-background-color="green">
                        <i class="material-icons">code</i>
                    </div>
                    <div class="card-content">
                        <h4 class="card-title">All Pages</h4>
                        <a href="<?php echo site_url('admin/pages/add');?>" class="btn btn-info pull-right"><i class="material-icons">add</i> Add Page</a>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive table-sales">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><strong>S.N</strong></th>
                                                <th><strong>Title</strong></th>
                                                <th class="text-right"><strong>Action</strong></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                                <?php
                                                if($pages)
                                                {
                                                    $offset = 1;
                                                    foreach($pages as $page)
                                                    {
                                                     ?>
                                                            <tr>
                                                                <td><?php echo $offset;?></td>
                                                            
                                                                <td><?php echo $page->title;?></td>
                                                                <td>
                                                                    <a title="Delete Page" href="<?php echo site_url('admin/pages/delete/'.$page->id);?>" class="btn btn-sm btn-danger pull-right" onclick="return confirm('Are you sure you want to delete this page?');">
                                                                        <i class="material-icons" >delete</i>
                                                                    </a>
                                                                    <a title="Edit Page" href="<?php echo site_url('admin/pages/add/'.$page->id);?>" class="btn btn-sm btn-success pull-right">
                                                                        <i class="material-icons">edit</i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        $offset++;
                                                        }
                                                       

                                                        
                                                }
                                                else
                                                    {
                                                        ?>
                                                        <td colspan="3">No pages found</td>
                                                        <?php
                                                    }
                                                   
                                                ?>
                                                </tbody>
                                    </table>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <nav aria-label="Page navigation" class="col-md-12 text-center">
                         <ul class="pagination">
                            <?php echo $this->pagination->create_links();?>
                        </ul>
                    </nav>
            </div>
        </div>
    </div>
</div>
</div>
