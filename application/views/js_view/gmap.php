<script>
/* 
* GMap V3 Geocoding 
* @author : Ambika
* 2012
*/
$(function(){
initialize_map();
})
var base_url = '<?php echo base_url();?>';
var geocoder = new google.maps.Geocoder();
  var map;
  var marker;
  var image = new google.maps.MarkerImage(
                base_url+'assets/images/map_pin.png'
              );
            
    var shadow = new google.maps.MarkerImage(
                base_url+'assets/images/shadow.png',
                new google.maps.Size(52,40),
                new google.maps.Point(0,0),
                new google.maps.Point(14,40)
              );
  function geocodePosition(pos) {
  geocoder.geocode({
    latLng: pos
  }, function(responses) {
    if (responses && responses.length > 0) {
      updateMarkerAddress(responses[0].formatted_address);
    } else {
      updateMarkerAddress('Cannot determine address at this location.');
    }
  });
}

function updateMarkerPosition(latlng) {
    //alert(latlng.lat());
    $('#latitude').val(latlng.lat());
    $('#longitude').val(latlng.lng());
}

function updateMarkerAddress(str) {
  document.getElementById('formattedAddress').value = str;
}
  function initialize_map() {
    var lat_value=document.getElementById('latitude').value; 
    var long_value=document.getElementById('longitude').value;
    //set latlang to Singha Durbar, Kathmandu
    if(lat_value=='0' || lat_value=="" )
    {
         lat_value=27.6982754;
    }
    if(long_value=='0' || long_value=='')
    {
         long_value=85.323064;
    }
    var latlng = new google.maps.LatLng(lat_value, long_value);
    var myOptions = {
      zoom: 14,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    marker = new google.maps.Marker({
    position: latlng,
    title: 'Singha Durbar, Kathmandu',
    map: map,
    draggable: false,
    icon: image,
    //shadow: shadow,
  });
   // Update current position info.
  //updateMarkerPosition(latlng);
  geocodePosition(latlng);
	// Add dragging event listeners.
   
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerPosition(marker.getPosition());
  });
 
  google.maps.event.addListener(marker, 'dragend', function() {
    geocodePosition(marker.getPosition());
  });
  }

  function codeAddress() {
    
    var address="";
    
    address=$(".location").val();
    if(address.replace('Kathmandu','')==address)    
    address+=', Kathmandu';
    
    geocoder.geocode( { 'address': address,'region':'NP','partialmatch': true}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
        marker.setMap(null);//clear the previous marker from the map
        latlng=results[0].geometry.location;
        map.setCenter(latlng);
            // Update current position info.
          updateMarkerPosition(latlng);
          geocodePosition(latlng);
            marker = new google.maps.Marker({
            map: map,
			draggable: true,
            position: results[0].geometry.location,
            icon: image,
            //shadow: shadow,
        });
        	// Add dragging event listeners.
 
  google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerPosition(marker.getPosition());
  });
 
  google.maps.event.addListener(marker, 'dragend', function() {
    geocodePosition(marker.getPosition());
  });
      } else {
        alert("Geocode could not locate the address. Please recheck your input again!" );
      }
    });
  }
  
  function updateMapPinPosition()
  {
    alert('test');
     marker.setMap(null);//clear the previous marker from the map
     var latlng=new google.maps.LatLng(document.getElementById('latitude').value,document.getElementById('longitude').value);
      map.setCenter(latlng);
     marker = new google.maps.Marker({
            map: map,
			draggable: true,
            position: latlng,
            icon: image,
            //shadow: shadow,
        });
    google.maps.event.addListener(marker, 'drag', function() {
    updateMarkerPosition(marker.getPosition());
  });
 
  google.maps.event.addListener(marker, 'dragend', function() {
    geocodePosition(marker.getPosition());
  });
  }
</script>