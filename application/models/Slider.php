<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends CI_Model {

	public function getActiveSlider()
	{
		$arr = $this->db->get_where('tbl_sliders',array('status'=>1))->result_array();
        foreach($arr as $k=>$a)
        {
            $arr[$k]['caption'] = $a['caption'];
            $arr[$k]['image'] = base_url().'assets/images/sliders/resized/'.$a['image'];
        }
        return $arr;
	}
    public function getById($id)
	{
		return $this->db->get_where('tbl_sliders',array('id'=>$id))->row();
	}
	public function countAll()
	{
		return $this->db->from("tbl_sliders")->count_all_results();
	}
	public function listSlider($limit,$offset)
	{
		$this->db->limit($limit,$offset);
        $this->db->order_by('id','DESC');
        return $this->db->get('tbl_sliders')->result();
	}
    public function deleteById($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('tbl_sliders');
    }
    
	public function saveSlider($id,$fields)
	{
		foreach($this->input->post() as $k=>$v)
        {
            if(in_array($k,$fields))
            {
                $arr[$k] = trim($v);
            }
        }
        if(!$id)
        {
            $str = 'added';
            if($arr){
                
                $this->db->insert('tbl_sliders', $arr); 
                }

        }
        else
        {
            $str = 'updated';
            if($arr){
                $this->db->where('id',$id);
                $this->db->update('tbl_sliders', $arr);
            } 
        }
        return $str;
	}
	
}