<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php if(isset($title))echo $title;else echo " EPL_T20 ";?></title>
<!--favicon-->
<link rel="icon" href="img/favicon.ico" type="image/x-icon">
<!-- Latest compiled and minified JavaScript -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
 <script src="<?php echo site_url('assets/bootstrap/js/bootstrap.js');?>"></script>
<!--bootstrap-->
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/bootstrap/css/bootstrap.css');?>">
<!--custom style-->
<link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/style.css');?>">
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=222640917866457";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
<!--------------HEADER---------->
    <header  class="navbar-fixed-top">
    <!---------------------------------------->
    <div class="container mobile-menu">
    <div class="navbar-brand Epl-logo-main" align="center">
          <a href="<?php echo site_url(); ?>"><img src="<?php echo site_url('assets/images/logo.png');?>"></a>
       </div>
       <div class="bg-cover" class="none"></div>  
        <a class="hamburger-shell">
            <div class="hamb top"></div>
            <div class="hamb middle"></div>
            <div class="hamb bottom"></div>
        
        <ul id="menu">
            <a href="<?php echo site_url();?>"><li>HOME</li></a>
            <a href="<?php echo site_url('pages/details/about-us');?>"><li>ABOUT US</li></a>
            <a href="<?php echo site_url('news'); ?>"><li>NEWS</li></a>
            <a href="<?php echo site_url('pages/contactUs'); ?>"><li>CONTACT US</li></a>
        </ul>
        </a>  
  </div>
    <!---------------------------------------->
      <div class="container fullscreen-nav">
          <div class="navbar-header">   
            <!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar" style="background-color:black;"></span>
                        <span class="icon-bar" style="background-color:black;"></span>
                        <span class="icon-bar" style="background-color:black;">  </span>                      
             </button>-->
             <div class="navbar-brand Epl-logo-main" align="center">
                    <a href="<?php echo site_url(); ?>"><img src="<?php echo site_url('assets/images/logo.png');?>"></a>
             </div>
            </div>
            <div id="toggle-nav">
               <div class="hamburger"></div>
            </div>
            <div class="collapse navbar-collapse head-nav-main" id="myNavbar">
                  <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>
                    <li><a href="<?php echo site_url('pages/details/about-us');?>">ABOUT US</a></li>
                    <li><a href="<?php echo site_url('news'); ?>">NEWS</a></li>
                    <li><a href="<?php echo site_url('pages/contactUs'); ?>">CONTACT US</a></li>
                  </ul>
        </div>
        </div>
    </header> 