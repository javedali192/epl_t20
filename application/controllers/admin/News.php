<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
    public function __construct()
    {
        
        parent::__construct();
        if(!$this->session->userdata('id'))
        {
            redirect('login');
        }
        $this->load->library('pagination');
        $this->load->library('Common');
        $this->load->model('NewsModel');
    } 
	public function index($offset=0)
	{
	    $data['title'] = 'News - '.$this->session->userdata('site_title');
        $config = $this->common->getPaginationConfig(base_url().'admin/news/index/');
        $config['total_rows'] = $this->NewsModel->countAllNews();
        $data['news'] = $this->NewsModel->listNews($config['per_page'],$offset);
        $this->pagination->initialize($config);
		$this->load->view('admin/include/header',$data);
        $this->load->view('admin/news/index',$data);
        $this->load->view('admin/include/footer');
	}
    public function add($id=0)
    {
        $this->load->library('Common');
        $fields = $this->common->fields('news');
        if($id == 0)
        {
            $news = $this->common->setBlankModel($fields);
        }
        else
        {
            $news = $this->NewsModel->getById($id);
        }

        if($this->input->post())
        {
            $status = $this->NewsModel->saveNews($id,$fields);
            $this->session->set_flashdata('message','News '.$status.' successfully');
            redirect('admin/news');
        }
        $data['model'] = $news;
        $data['title'] = 'Add/Edit News - '.$this->session->userdata('site_title');
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/news/add',$data);
        $this->load->view('admin/include/footer');
    }
    
    public function delete($id)
    {
        $this->NewsModel->deleteById($id);
        $this->session->set_flashdata('message','News has been deleted successfully');
        redirect('admin/news');
    }
    public function upload()
    {
        $this->load->library('Common');
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'])
        {
            $name = $_FILES['file']['name'];
            $arr_files = explode('.',$name);
            $ext = end($arr_files);
            $valid = $this->common->checkValidExt($ext);
            if($valid)
            {
                $arr['file'] = date('YmdHis').'_'.rand(1,999999).'.'.$ext;
                if(move_uploaded_file($_FILES['file']['tmp_name'],APPPATH.'../assets/images/news/full/'.$arr['file'])){
                $arr['error'] = false;
                $config = $this->common->getNewsConfig('news',$arr['file']);
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $this->image_lib->clear();
                $this->saveImageDefault($arr['file']);
                }
                else
                {
                    $arr['error'] = 'Could not upload the file';
                    $arr['file'] = false;
                }
            }
            else
            {
                $arr['error'] = 'Invalid File Extension';
                $arr['file'] = false;
                
            }
            
        }
        else
        {
            $arr['error'] = 'Unknown Error';
            $arr['file'] = false;
        }
        echo json_encode($arr);die();
    }
    public function saveImage()
    {
        $file = $_POST['file'];
        echo $config['x_axis'] = $_POST['x'];
        echo "<br/>";
        echo $config['y_axis'] = $_POST['y'];
        echo "<br/>";
        echo $config['width'] = (int)$_POST['w'];
        echo "<br/>";        
        echo $config['height'] = (int)$_POST['h'];
        $config['image_library'] = 'GD2';
        $config['source_image'] = APPPATH.'../assets/images/news/main/'.$file;
        $config['new_image'] = APPPATH.'../assets/images/news/resized/'.$file;
        $config['maintain_ratio'] = FALSE;
        $config['create_thumb'] = FALSE;
        
        //var_dump($config);die();
        $this->load->library('image_lib', $config);
        if ( ! $this->image_lib->crop())
        {
                echo $this->image_lib->display_errors();
        }
        else
        {
            $this->final_resize($config['new_image']);
        }
        //$this->image_lib->crop();
        
    }
    public function final_resize($new_image)
    {
        $config['image_library'] = 'gd2';
        $config['source_image'] = $new_image;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['width']  = 800;
        $config['height'] = 450;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
    public function saveImageDefault($file)
    {
        $this->load->library('image_lib');
        $config['x_axis'] = '0';
        $config['y_axis'] = '0';
        $config['width'] = '800';      
        $config['height'] = '450';
        $config['image_library'] = 'GD2';
        $config['source_image'] = APPPATH.'../assets/images/news/main/'.$file;
        $config['new_image'] = APPPATH.'../assets/images/news/resized/'.$file;
        $config['maintain_ratio'] = FALSE;
        $config['create_thumb'] = FALSE;
        $this->image_lib->initialize($config);
        $this->image_lib->crop();
        
    }
}
