<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Model {

	public function getLoggedInUser()
    {
        $id = $this->session->userdata('id');
        return $this->db->get_where('tbl_admin',array('id'=>$id))->row();
    }
    public function saveData($arr)
    {
        $this->db->where('id',$this->session->userdata('id'));
        $this->db->update('tbl_admin',$arr);
    }
}