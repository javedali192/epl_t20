<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_Gjdm_0nJk17UVBPoV5Im40uQeguoRAo"></script>
<?php $this->load->view('js_view/gmap.php');?>
<!--------------NEWS AND ASIDE------------>
<section class="section_wrapper">
  <div class="news-quicklink-main">
      <div class="container">
          <div class="row">
              <div class="col-lg-9 news-body">
                  <div class="main-heading"><h3>Contact Us</h3></div>
                      <div class="row">
                      
                            <div class="col-lg-12">
                                <div class="row">
                  <div class="col-md-12 news-text">
                      <div class="col-md-12">
                        <?php echo $this->session->flashdata('msg'); ?>
                      </div>
                     
                    <div class="row">
                      <div class="col-md-12">
                        <h3 class="col-md-12">Contact Details</h3>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                              <div>
                                  <b><i class="fa fa-mobile"></i></b>
                                  <?php echo "+977-".$mContactus->phone; ?>
                              </div>
                              <div>
                                  <b><i class="fa fa-envelope-o"></i></b>
                                  <?php echo $mContactus->email; ?>
                              </div>
                              <div>
                                  <b><i class="fa fa-location-arrow"></i></b>
                                  <?php echo $mContactus->location; ?>
                              </div> 
                              <div>
                                  <b><a href="<?php echo $mContactus->facebook;?>" target="_blank" ><i class="fa fa-facebook-official sn"></i></a></b>
                                  <b><a href="<?php echo $mContactus->twitter;?>" target="_blank"><i class="fa fa-twitter sn"></i></a></b>
                              </div>                      
                        </div>  
                      </div>
                    </div>
                    <div class="martop-15"></div>
                    <div class="row">
                      <div class="col-md-12">
                            <h3 class="col-md-12">Send Us Message</h3>
                            <div class="clearfix"></div>
                            <form action="" method="post" enctype="multipart/form-data" id="settings">
                                <div class="col-md-12">
                                      <div class="form-group label-floating">
                                          <label class="control-label">Full name:</label>
                                          <input type="text" class="form-control" required name="name">
                                      </div>
                                </div>                                         
                                <div class="clearfix"></div>

                                <div class="col-md-12">
                                      <div class="form-group label-floating">
                                          <label class="control-label">Email:</label>
                                          <input type="text" class="form-control" required name="email">
                                      </div>
                                </div>                                         
                                <div class="clearfix"></div>

                                <div class="col-md-12">
                                      <div class="form-group label-floating">
                                          <label class="control-label">Phone No:</label>
                                          <input type="text" class="form-control" required name="contact">
                                      </div>
                                </div>                                         
                                <div class="clearfix"></div>

                                <div class="col-md-12">
                                      <div class="form-group label-floating">
                                          <label class="control-label">Subject:</label>
                                          <input type="text" class="form-control" required name="subject">
                                      </div>
                                </div>                                         
                                <div class="clearfix"></div>

                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label class="control-label" >Message:</label>
                                        <textarea rows="5" class="form-control" name="message" required></textarea>
                                    </div>
                                </div> 
                                <div class="clearfix"></div>
                                
                                <div class="col-md-12 send-btn">
                                    <button type="submit" class="btn btn-primary">SEND</button>
                                </div>
                                
                                 <div class="col-md-12 map">
                                        <div class="clearfix"></div>
                                        <input type="hidden" name="latitude" value="<?php if($mContactus->latitude)echo $mContactus->latitude;else echo "27.6982754"?>" id="latitude"  />
                                        <input type="hidden" name="longitude" value="<?php if($mContactus->longitude)echo $mContactus->longitude;else echo "85.323064"?>" id="longitude"  />
                                        <div id="map_canvas" style="height: 200px;background:#e5e5e5;margin-top:15px;"></div>
                                    </div>
                                    <div class="clearfix">
                                  </div>                         
                                
                                <div class="clearfix"></div>
                            </form>        
                      </div>
                    </div>
                     <div clearfix></div>
                  </div>
                </div> 
                                
                            </div>
                               
                        </div>
                    </div>
                <?php $this->load->view('common/sidebar');?>
            </div>
        </div>
    </div>
</section>
