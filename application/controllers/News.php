<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
    public function __construct()
    {
        
        parent::__construct();
        
        $this->load->library('pagination');
        $this->load->library('Common');
        $this->load->model('NewsModel');
    } 
	public function index($offset=0)
	{
	    $data['title'] = 'News - '.$this->session->userdata('site_title');
        $config = $this->common->getPaginationConfig(base_url().'news/index/');
        $config['total_rows'] = $this->NewsModel->countAllNews();
        $data['news'] = $this->NewsModel->listNews($config['per_page'],$offset);
        $data['mTime'] = $this->NewsModel;
        $this->pagination->initialize($config);
		$this->load->view('common/header',$data);
        $this->load->view('news/index',$data);
        $this->load->view('common/footer');
	}
    public function details($slug="")
    {
        $data['title'] = $slug." - ".$this->session->userdata('site_title');
        $data['details'] = $this->NewsModel->getNewsBySlug($slug);
        $this->load->view('common/header',$data);
        $this->load->view('news/details',$data);
        $this->load->view('common/footer');    
    }
   
}
