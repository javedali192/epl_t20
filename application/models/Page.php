<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Model {

    private $table_name = 'tbl_pages';

    public function getActive()
    {
        return $this->db->get_where($this->table_name,array('is_active'=>1))->result();
    }
    public function getAll($limit,$offset)
    {
        $this->db->select('child.*,parent.title as parent_title,parent.id as pid');
        $this->db->from('tbl_pages child');
        $this->db->join('tbl_pages parent', 'child.parent_id = parent.id', 'left');
        $this->db->limit($limit,$offset);
        //$this->db->where(array('child.parent_id >'=>0));
        $this->db->order_by('pid','DESC');
        $arr = $this->db->get()->result();
        return $arr;

    }
    public function listAllPages($limit,$offset)
    {
       
        $this->db->limit($limit,$offset);
        //$this->db->where(array('child.parent_id >'=>0));
        $this->db->order_by('id','DESC');
        $arr = $this->db->get_where('tbl_pages',array('parent_id'=>0))->result();
        return $arr;

    }
    public function getAboutUs()
    {
        $result = $this->db->get_where('tbl_pages',array('slug'=>'about-us'))->row();
        return $result;
    }
    public function getPageBySlug($slug)
    {
         return $this->db->get_where('tbl_pages',array('slug'=>$slug))->row();
    }
    public function getByAttributes($arr)
    {
        return $this->db->get_where($this->table_name,$arr)->row();
    }
    public function getAllByAttributes($arr)
    {
        if($arr)
            return $this->db->get_where($this->table_name,$arr)->result();
        else
            return $this->db->get($this->table_name)->result();
    }
    public function countAll()
    {
        $this->db->where('parent_id',0);
        return $this->db->from($this->table_name)->count_all_results();
    }
    public function countAllByAttributes($arr)
    {
        return $this->db->from($this->table_name)->where($arr)->count_all_results();
    }
    public function getByOrder($arr)
    {
        foreach($arr as $k=>$a)
        {
            $this->db->order_by($k,$a);
        }
        $this->db->get($this->table_name)->result();
    }
   
    public function saveData($id,$fields)
    {
        foreach($this->input->post() as $k=>$v)
        {
            if(in_array($k,$fields))
            {
                $arr[$k] = trim($v);
            }
        }
        
        
        if(!$id)
        {
            $str = 'added';
            if($arr){
                $arr['slug'] = $this->generateSlug($arr['title']);
                $this->db->insert($this->table_name, $arr); 
                }

        }
        else
        {
            $str = 'updated';
            if($arr){
                
                $this->db->where('id',$id);
                $this->db->update($this->table_name, $arr);
            } 
        }
        return $str;
    }
    public function deleteByAttr($arr)
    {
        $this->db->where($arr);
        $this->db->delete($this->table_name);

    }
    public function generateSlug($title)
    {
        $slug = url_title($title);
        while($this->db->get_where($this->table_name,array('slug'=>$slug))->row())
        {
            $slug = $slug.'_'.rand(1,999);
        }
        return $slug;
    }
    public function getAboutContent()
    {
        $row = $this->db->get_where('tbl_pages',array('slug'=>'about-us'))->row();
        $desc = strip_tags($row->description);
        return $desc = substr($desc,0,500);
    }
	
}