<!--------------NEWS AND ASIDE------------>
<section class="section_wrapper">
  <div class="news-quicklink-main">
      <div class="container">
          <div class="row">
              <div class="col-lg-9 news-body">
                  <div class="main-heading"><h3><?php echo $details->title; ?></h3></div>
                      <div class="row news-main">
                      <?php
                        if($details)
                        {
                          
                            ?>
                            <div class="col-lg-12">
                              <div class="full-news-img"><img src="<?php echo site_url('assets/images/news/resized/'.$details->image); ?>"></div>
                                <span class="full-news-date"><?php echo date("D, d M Y",strtotime($details->publish_date));?></span>
                                <div class="full-news-description"><?php echo $details->description; ?></div>
                            </div>
                            <?php
                         }
                      ?>
                            
                            
                        </div>
                        <div class="pagination">
                           <?php echo $this->pagination->create_links();?>  
                        </div>
                    </div>
                <?php $this->load->view('common/sidebar');?>
            </div>
        </div>
    </div>
</section>