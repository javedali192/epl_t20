<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><?php if(isset($title))echo $title;else echo "Admin - ".$this->session->userdata('site_title');?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    
    <!-- Bootstrap core CSS     -->
    <link href="<?php echo site_url('assets/css/bootstrap.min.css');?>" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo site_url('assets/css/material-dashboard.css');?>" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo site_url('assets/css/demo.css');?>" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="<?php echo site_url('assets/css/fa/css/font-awesome.min.css')?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    <script src="<?php echo site_url('assets/js/jquery.js');?><?php //echo site_url('assets/js/jquery-3.1.1.min.js');?>" type="text/javascript"></script>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-active-color="rose" data-background-color="<?php if($this->session->userdata('site_color'))echo $this->session->userdata('site_color');else echo "white";?>" data-image="<?php echo site_url('assets/img/sidebar-1.jpg');?>">
            <!--
        Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
        Tip 2: you can also add an image using data-image tag
        Tip 3: you can change the color of the sidebar with data-background-color="white | black"
    -->
            
            <div class="logo logo-mini">
                <a href="<?php echo site_url('admin');?>" class="simple-text">
                    <?php echo $this->session->userdata('site_title');?>
                </a>
            </div>
            <div class="sidebar-wrapper">
                <div class="user">
                    <?php 
                        //var_dump($this->session->userdata());
                        if($this->session->userdata('site_logo'))
                        {
                            $logo = $this->session->userdata('site_logo');
                        }
                        else
                        $logo = 'logo.png';
                        ?>
                        <img src="<?php echo site_url('assets/img/faces/'.$logo);?>" class="img-responsive" />
                    
                </div>
                <ul class="nav">
                    <li class="active">
                        <a href="<?php echo site_url('admin/dashboard');?>">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    
                   
                    
                    <li>
                        <a data-toggle="collapse" href="#slider">
                            <i class="material-icons">code</i>
                            <p>Sliders
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="slider">
                            <ul class="nav">
                                <li>
                                    <a href="<?php echo site_url('admin/sliders')?>">List All</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('admin/sliders/add')?>">Add New</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    
                    <li>
                        <a data-toggle="collapse" href="#pages">
                            <i class="material-icons">description</i>
                            <p>Pages
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="pages">
                            <ul class="nav">
                                <li>
                                    <a href="<?php echo site_url('admin/pages')?>">List All</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('admin/pages/add')?>">Add New</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#news">
                            <i class="fa fa-file-text"></i>
                            <p>News
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="news">
                            <ul class="nav">
                                <li>
                                    <a href="<?php echo site_url('admin/news')?>">List All</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('admin/news/add')?>">Add New</a>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a data-toggle="collapse" href="#profile">
                            <i class="material-icons">power_settings_new</i>
                            <p>Profile
                                <b class="caret"></b>
                            </p>
                        </a>
                        <div class="collapse" id="profile">
                            <ul class="nav">
                            <?php
                                if($this->session->userdata('role')=='1')
                                {
                                ?>
                                <li>
                                    <a href="<?php echo site_url('admin/dashboard/settings')?>">Settings</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('admin/dashboard/theme')?>">Theme</a>
                                </li>
                                <?php
                                }
                            ?>
                                <li>
                                    <a href="<?php echo site_url('admin/dashboard/logout');?>">Logout</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    
                    
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Dashboard - <?php echo $this->session->userdata('site_title');?> </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                                                       
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li>
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>
                        <!--
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group form-search is-empty">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>-->
                    </div>
                </div>
            </nav>
            <?php
            $message = $this->session->flashdata('message');
            if($message)
            {
                ?>
                    <div class="col-md-12">
                        <div class="col-md-12 flashdata">
                            <div class="alert alert-success" role="alert"><?php echo $message;?></div>
                        </div>
                    </div>
                <?php
            }
            ?>
            
            