<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">code</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">All Sliders</h4>
                                    <a href="<?php echo site_url('admin/sliders/add');?>" class="btn btn-sm btn-info pull-right"><i class="material-icons">add</i> Add new</a>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive table-sales">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th><strong>Image</strong></th>
                                                            <th><strong>Caption</strong></th>
                                                            <th><strong>Link</strong></th>
                                                            <th><strong>Action</strong></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if($sliders)
                                                        {
                                                            foreach($sliders as $slider)
                                                            {
                                                              ?>
                                                              <tr>
                                                                
                                                                <td class="preeti small checkunicode" style="width: 33%;">
                                                                    <img src="<?php echo site_url('assets/images/sliders/resized/'.$slider->image);?>" class="img-responsive" />
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                        echo substr($slider->caption,0,50).'...';
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <?php
                                                                        if($slider->link)
                                                                        {
                                                                            ?>
                                                                            <a href="<?php echo $slider->link;?>"><?php echo $slider->link;?></a>
                                                                            <?php
                                                                        }
                                                                       
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <a title="Edit Slider" href="<?php echo site_url('admin/sliders/add/'.$slider->id);?>" class="btn btn-sm btn-success"><i class="material-icons">edit</i></a>
                                                                    <a title="Delete Slider" href="<?php echo site_url('admin/sliders/delete/'.$slider->id);?>" class="btn btn-sm btn-primary" onclick="return confirm('Are you sure you want to delete this slider?')"><i class="material-icons">delete</i></a>
                                                                </td>
                                                            </tr>
                                                              <?php  
                                                            }
                                                            
                                                        }
                                                        else
                                                        {
                                                            ?>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <p>No slider found</p>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <nav aria-label="Page navigation" class="col-md-12 text-center">
                                     <ul class="pagination">
                                        <?php echo $this->pagination->create_links();?>
                                    </ul>
                                </nav>
                        </div>
                    </div>
                </div>
</div>
</div>