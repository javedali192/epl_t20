<script>
            function showCoords(c)
            {
                $('.x').val(c.x);
                $('.y').val(c.y);
                $('.w').val(c.w);
                $('.h').val(c.h);
              // variables can be accessed here as
              // c.x, c.y, c.x2, c.y2, c.w, c.h
            }
            function initiate_ajax_upload1(button_id) {
                //alert(button_id);
                var button = $('#' + button_id), interval;
                
                new AjaxUpload(button, {
                    action: '<?php echo site_url('admin/sliders/upload')?>',
                    name: 'file',
                    onSubmit: function (file, ext) {
                        button.text('Uploading');
                        this.disable();
                        interval = window.setInterval(function () {
                            var text = button.text();
                            if (text.length < 13) {
                                button.text(text + '.');
                            } else {
                                button.text('Uploading');
                            }
                        }, 200);
                    },
                    onComplete: function (file, response) {
                            this.enable();
                            button.html('Browse');
                            var obj = JSON.parse(response);
                            if(obj.error)
                            {
                                alert(obj.error);
                            }
                            else
                            {
                                $('.image_name').val(obj.file);
                                $('.img-holder-slider').html('<img src="<?php echo base_url().'assets/images/sliders/full/';?>'+obj.file+'" style="max-width:100%;" />');
                                $('#toCrop').html('<img src="<?php echo base_url().'assets/images/sliders/main/';?>'+obj.file+'" />');
                                $('#toCrop img').Jcrop({
                                    onSelect: showCoords,
                                    onChange: showCoords,
                                    setSelect:   [ 0, 0, 1170 , 400],
                                    aspectRatio: 117 / 40,
                                    minSize: [1170 / 400]
                                });
                            }
                            window.clearInterval(interval);
        
                        
                    }
                });
            }
            $(function(){
                <?php
                if($id)
                {
                    ?>
                    $('#toCrop img').Jcrop({
                                    onSelect: showCoords,
                                    onChange: showCoords,
                                    setSelect:   [ 0, 0, 1170 , 400 ],
                                    aspectRatio: 117 / 40,
                                    minSize: [1170 / 400]
                                });
                    <?php
                }
                ?>
                
                initiate_ajax_upload1('upload'); 
                $('#saveImage').click(function(){
                    $(this).text('Saving');
                    $(this).attr('disabled','disabled');
                    interval = window.setInterval(function () {
                        var text = $(this).text();
                        if (text.length < 11) {
                            $(this).text(text + '.');
                        } else {
                            $(this).text('Saving');
                        }
                    }, 200);
                    $.ajax({
                        data:'x='+$('.x').val()+'&y='+$('.y').val()+'&w='+$('.w').val()+'&h='+$('.h').val()+'&file='+$('.image_name').val(),
                        type:'post',
                        url:'<?php echo site_url('admin/sliders/saveImage')?>',
                        success:function()
                        {
                            $('#saveImage').removeAttr('disabled');
                            $('#saveImage').html('Save Image');
                            $('#cropModal').modal('hide');  
                            window.clearInterval(interval);   
                            $('.img-holder-slider').html('<img src="<?php echo base_url().'assets/images/sliders/resized/';?>'+$('.image_name').val()+'?rand='+Math.random()+'" style="max-width:100%;" />')                       
                        }
                    });
                })
                $( "#settings" ).validate({
                  rules: {
                    new_password: "required",
                    confirm_password: {
                      equalTo: "#new_password"
                    }
                  }
                });

            })
            
            </script>