<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NewsModel extends CI_Model {

	public function getActiveNews()
	{
		$arr = $this->db->get_where('news',array('status'=>1))->result_array();
        foreach($arr as $k=>$a)
        {
            $arr[$k]['image'] = base_url().'assets/images/news/resized/'.$a['image'];
        }
        return $arr;
	}
    public function getHomeNews()
    {
        $this->db->limit(6);
        $arr = $this->db->get('news')->result();
        return $arr;
    }
    public function getNewsBySlug($slug)
    {
        $arr = $this->db->get_where('news',array('slug'=>$slug))->row();
        return $arr; 
    }
    public function timeElapsed($time)
    {
        $time = time() - $time; // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array (
                        31536000 => 'year',
                        2592000 => 'month',
                        604800 => 'week',
                        86400 => 'day',
                        3600 => 'hour',
                        60 => 'minute',
                        1 => 'second'
                        );

            foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }
    }
	public function getById($id)
	{
		return $this->db->get_where('news',array('id'=>$id))->row();
	}
	public function countAllNews()
	{
		return $this->db->from("news")->count_all_results();
	}
	public function listNews($limit,$offset)
	{
		$this->db->limit($limit,$offset);
        $this->db->order_by('id','DESC');
        return $this->db->get('news')->result();
	}
    public function deleteById($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('news');
    }
    
	public function saveNews($id,$fields)
	{
		foreach($this->input->post() as $k=>$v)
        {
            if(in_array($k,$fields))
            {
                $arr[$k] = trim($v);
            }
        }
        if(!$id)
        {
            $str = 'added';
            if($arr){
                $arr['publish_date'] = date('Y-m-d h:i:s');
                $this->db->insert('news', $arr);
                $id = $this->db->insert_id();
                $this->saveSlug($arr['title'],$id); 
                }

        }
        else
        {
            $str = 'updated';
            if($arr){
                $arr['publish_date'] = date('Y-m-d h:i:s');
                $this->db->where('id',$id);
                $this->db->update('news', $arr);
                $this->saveSlug($arr['title'],$id);
            } 
        }
        return $str;
	}
    public function saveSlug($title,$id)
    {
        $title = strtolower(url_title($title));
        while($this->db->get_where('news',array('slug'=>$title,'id !='=>$id))->row())
        {
            $title = $title.rand(1,999);
        }
        $this->db->where('id',$id)->update('news',array('slug'=>$title));
    }
	
}