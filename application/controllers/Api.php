<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    public function __construct()
    {        
        parent::__construct();
        $this->load->library('Common');
        $this->load->model('SmsType');
        $this->load->model('SmsM');
    } 
    public function documentation()
    {
        $this->load->view('api/documentation');
    }
    public function sendRequest($id=0)
    {
        
        $this->load->library('Common');
        $fields = $this->common->fields('tbl_sms');
        if(isset($_GET['sender']) && isset($_GET['text']))
        {
            $arr['phone_number'] = $_GET['sender'];
            $from = $arr['phone_number']; 
            $arr['message'] = $_GET['text'];
            $msg = str_replace(array(' ','+'),array('%20','%20'),$arr['message']);
            
            $arr['created_at'] = date('Y-m-d H:i:s');
            $arr['status'] = $this->sendToMeroTV('https://cv01.panaccess.com/?requestMode=function&f=pushSparrowSms&account=f76HjkgFhhZZU83G&from='.$from.'&to=12345&keyword=mero&text='.$msg);
            $this->SmsM->saveData($id,$fields,$arr);
            echo $arr['status'];die();
            //}
        }
        else
        {
            $arr['phone_number'] = '';
            $arr['message'] = '';
            $arr['status'] = 'fail:Sender and text cannot be blank';
        }
        echo json_encode($arr);
    }
    public function getOutlets()
    {
        $this->load->model('Outlet');
        $result = $this->Outlet->getActiveOutlets();   
        echo json_encode($result);die();
    }
    
    public function getSliders()
    {
        $this->load->model('Slider');
        $result = $this->Slider->getActiveSlider();        
        echo json_encode($result);die();
    }
    public function getOffers()
    {
        $this->load->model('Offer');
        $result = $this->Offer->getActiveOffer();  
        echo json_encode($result);die();
    }
    public function getCableOperators()
    {
        $this->load->model('Operator');
        $result = $this->Operator->getActiveOperators();
        echo json_encode($result);die();
    }
    public function getPackages()
    {
        ini_set("xdebug.var_display_max_children", -1);
        ini_set("xdebug.var_display_max_data", -1);
        ini_set("xdebug.var_display_max_depth", -1);
        $this->load->model('Package');
        $this->load->model('Channel');
        $channel = $this->Channel;
        $result = $this->Package->getActivePackage($channel);     
        var_dump($result);
        echo json_encode($result);die();
    }
    public function getPages()
    {
        ini_set("xdebug.var_display_max_children", -1);
        ini_set("xdebug.var_display_max_data", -1);
        ini_set("xdebug.var_display_max_depth", -1);
        $this->load->model('Category');
        $this->load->model('Page');
        $cats = $this->Category->getActiveCategory();
        if($cats)
        {
            $i=0;
            foreach($cats as $c)
            {
                
                $arr[$i]['category'] = $c->title;
                $arr[$i]['pages'] = $this->Page->getActivePageByCat($c->id);
                $i++; 
            }
        }
       
        echo json_encode($arr);die();
    }
    function sendToMeroTV($base){
        $curl = curl_init();
        $agent = 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:52.0) Gecko/20100101 Firefox/52.0';
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: ";
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        
    
        curl_setopt($curl, CURLOPT_URL, $base);
        curl_setopt($curl, CURLOPT_REFERER, $base);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 300);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        $str = curl_exec($curl);
        curl_close($curl);
        return $str;
    }
}