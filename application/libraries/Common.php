<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Common
{
    public function fields($tbl)
    {
        $CI =& get_instance(); 
        return $CI->db->list_fields($tbl);
    }
    public function setBlankModel($fields)
    {
    	$obj = new stdClass();
    	foreach($fields as $f)
    	{
    		$obj->$f = '';
    	}
    	return $obj;
    }
    public function getUserName($id)
    {
        //echo $id;die();
        $CI =& get_instance(); 
        return $CI->db->get_where('tv_admin',array('id'=>$id))->row()->fullname;
    }
    public function getHomePages()
    {
        $CI =& get_instance();
        $CI->db->where('slug !=','about-us');
        $CI->db->where('parent_id',0);
        $arr = $CI->db->get('tbl_pages')->result();
        return $arr;

    }
    public function getPaginationConfig($url)
    {
        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '<li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '<li>';
        $config['base_url'] = $url;
        $config['per_page'] = 6;
        return $config;
    }
    
    public function checkValidExt($ext)
    {
        $ext = strtolower($ext);
        $arr = array('jpg','jpeg','png','gif');
        if(in_array($ext,$arr))
        return true;
        else
        return false;
    }
    public function getSliderConfig($type,$file)
    {
        $config = array();
        if($type='slider')
        {
            $config['image_library'] = 'gd2';
            $config['source_image'] = APPPATH.'../assets/images/sliders/full/'.$file;
            $config['new_image'] = APPPATH.'../assets/images/sliders/main/'.$file;
            $config['maintain_ratio'] = TRUE;
            $config['width']         = 1170;
        }
        return $config;
    }
     public function getNewsConfig($type,$file)
    {
        $config = array();
        if($type='news')
        {
            $config['image_library'] = 'gd2';
            $config['source_image'] = APPPATH.'../assets/images/news/full/'.$file;
            $config['new_image'] = APPPATH.'../assets/images/news/main/'.$file;
            $config['maintain_ratio'] = TRUE;
            $config['width']         = 1000;
        }
        return $config;
    }
    public function getArticleConfig($type,$file)
    {
        $config = array();
        if($type='celebrities')
        {
            $config['image_library'] = 'gd2';
            $config['source_image'] = APPPATH.'../assets/images/articles/full/'.$file;
            $config['new_image'] = APPPATH.'../assets/images/articles/main/'.$file;
            $config['maintain_ratio'] = TRUE;
            $config['width']         = 900;
        }
        return $config;
    }
    public function getSetting()
    {
        $CI =& get_instance(); 
        return $CI->db->get('tbl_settings')->row();
    }
    public function getSliders()
    {
        $CI =& get_instance(); 
        return $CI->db->get('tbl_sliders')->result();
    }
    public function getCategories()
    {
        $CI =& get_instance();
        return $CI->db->get_where('tbl_category',array('is_active'=>1))->result();
    }
}