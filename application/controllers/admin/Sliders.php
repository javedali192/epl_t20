<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sliders extends CI_Controller {
    public function __construct()
    {
        
        parent::__construct();
        if(!$this->session->userdata('id'))
        {
            redirect('login');
        }
        $this->load->library('pagination');
        $this->load->library('Common');
        $this->load->model('Slider');
    } 
	public function index($offset=0)
	{
        $config = $this->common->getPaginationConfig(base_url().'admin/sliders/index/');
        $config['total_rows'] = $this->Slider->countAll();
        $data['sliders'] = $this->Slider->listSlider($config['per_page'],$offset);
        $this->pagination->initialize($config);
        $data['title'] = 'Sliders - '.$this->session->userdata('site_title');
		$this->load->view('admin/include/header',$data);
        $this->load->view('admin/sliders/index',$data);
        $this->load->view('admin/include/footer');
	}
    public function add($id=0)
    {
        $this->load->library('Common');
        $fields = $this->common->fields('tbl_sliders');
        $data['fields'] = $fields;
        if($this->input->post())
        {
            $status = $this->Slider->saveSlider($id,$fields);
            $this->session->set_flashdata('message','Slider '.$status.' successfully');
            redirect('admin/sliders');
        }
        if($id == 0)
        {
            $slider = $this->common->setBlankModel($fields);
        }
        else
        {
            $slider = $this->Slider->getById($id);
        }
        $data['model'] = $slider;
        $data['title'] = 'Add/Edit Slider - '.$this->session->userdata('site_title');
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/sliders/add',$data);
        $this->load->view('admin/include/footer');
    }

    public function delete($id)
    {
        $this->Slider->deleteById($id);
        $this->session->set_flashdata('message','Slider has been deleted successfully');
        redirect('admin/sliders');
    }
    public function upload()
    {
        $this->load->library('Common');
        if(isset($_FILES['file']['name']) && $_FILES['file']['name'])
        {
            $name = $_FILES['file']['name'];
            $arr_files = explode('.',$name);
            $ext = end($arr_files);
            $valid = $this->common->checkValidExt($ext);
            if($valid)
            {
                $arr['file'] = date('YmdHis').'_'.rand(1,999999).'.'.$ext;
                if(move_uploaded_file($_FILES['file']['tmp_name'],APPPATH.'../assets/images/sliders/full/'.$arr['file'])){
                $arr['error'] = false;
                $config = $this->common->getSliderConfig('sliders',$arr['file']);
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $this->image_lib->clear();
                $this->saveImageDefault($arr['file']);
                }
                else
                {
                    $arr['error'] = 'Could not upload the file';
                    $arr['file'] = false;
                }
            }
            else
            {
                $arr['error'] = 'Invalid File Extension';
                $arr['file'] = false;
                
            }
            
        }
        else
        {
            $arr['error'] = 'Unknown Error';
            $arr['file'] = false;
        }
        echo json_encode($arr);die();
    }
    public function saveImage()
    {
        $file = $_POST['file'];
        echo $config['x_axis'] = $_POST['x'];
        echo "<br/>";
        echo $config['y_axis'] = $_POST['y'];
        echo "<br/>";
        echo $config['width'] = (int)$_POST['w'];
        echo "<br/>";        
        echo $config['height'] = (int)$_POST['h'];
        $config['image_library'] = 'GD2';
        $config['source_image'] = APPPATH.'../assets/images/sliders/main/'.$file;
        $config['new_image'] = APPPATH.'../assets/images/sliders/resized/'.$file;
        $config['maintain_ratio'] = FALSE;
        $config['create_thumb'] = FALSE;
        
        //var_dump($config);die();
        $this->load->library('image_lib', $config);
        if ( ! $this->image_lib->crop())
        {
                echo $this->image_lib->display_errors();
        }
        //$this->image_lib->crop();
        
    }
    public function saveImageDefault($file)
    {
        $this->load->library('image_lib');
        $config['x_axis'] = '0';
        $config['y_axis'] = '0';
        $config['width'] = '1170';      
        $config['height'] = '400';
        $config['image_library'] = 'GD2';
        $config['source_image'] = APPPATH.'../assets/images/sliders/main/'.$file;
        $config['new_image'] = APPPATH.'../assets/images/sliders/resized/'.$file;
        $config['maintain_ratio'] = FALSE;
        $config['create_thumb'] = FALSE;
        $this->image_lib->initialize($config);
        $this->image_lib->crop();
        
    }
}
