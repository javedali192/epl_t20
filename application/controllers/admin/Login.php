<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('id'))
        {
            redirect('admin/dashboard');
        }
    } 
	public function index()
	{
		$this->load->view('admin/login');
	}
    public function verify()
    {
        if($this->input->post())
        {
            //var_dump($this->input->post());die();
            $username = $this->input->post('username');
            $password = sha1($this->input->post('password'));
            $row = $this->db->get_where('tbl_admin',array('username'=>$username,'password'=>$password))->row();
            if($row)
            {
                
                    $this->session->set_userdata('id',$row->id);
                    $this->session->set_userdata('username',$row->username);
                    $this->session->set_userdata('email',$row->email);
                    $this->session->set_userdata('role','1');
                    $this->load->model('DashboardM');
                    $setting = $this->DashboardM->getSetting();
                    if($setting)
                    {
                        $this->session->set_userdata('site_color',$setting->site_color);
                        $this->session->set_userdata('site_title',$setting->site_title);
                        $this->session->set_userdata('site_logo',$setting->site_logo);
                    }
                    redirect('admin/dashboard');
                
            }
            else
            {
                $row = $this->db->get_where('tbl_editors',array('username'=>$username,'password'=>$password))->row();
                if($row)
                {
                        $this->session->set_userdata('id',$row->id);
                        $this->session->set_userdata('username',$row->username);
                        $this->session->set_userdata('email',$row->email);
                        $this->session->set_userdata('role','2');
                        $this->load->model('DashboardM');
                        $setting = $this->DashboardM->getSetting();
                        if($setting)
                        {
                            $this->session->set_userdata('site_color',$setting->site_color);
                            $this->session->set_userdata('site_title',$setting->site_title);
                            $this->session->set_userdata('site_logo',$setting->site_logo);
                        }
                        redirect('admin/dashboard');
                    
                }
            }
            $this->session->set_flashdata('error','Invalid Username or Password');
            redirect('admin/login');
        }
    }
}
