<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>API 1.0 - Mero TV</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico">  
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!-- Global CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets_documentation/plugins/bootstrap/css/bootstrap.min.css">   
    <!-- Plugins CSS -->    
    <link rel="stylesheet" href="<?php echo base_url();?>assets_documentation/plugins/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets_documentation/plugins/prism/prism.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets_documentation/plugins/lightbox/dist/ekko-lightbox.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets_documentation/plugins/elegant_font/css/style.css">

    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="<?php echo base_url();?>assets_documentation/css/styles.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head> 

<body class="body-blue">
    <div class="page-wrapper">
        <!-- ******Header****** -->
        <header id="header" class="header">
            <div class="container">
                <div class="branding">
                    <h1 class="logo">
                        <a href="<?php echo site_url('api/documentation');?>">
                            <span aria-hidden="true" class="icon_documents_alt icon"></span>
                            <span class="text-highlight">Mero TV</span><span class="text-bold">API Documentation</span>
                        </a>
                    </h1>
                </div><!--//branding-->
                <ol class="breadcrumb">
                    <li class="active">V1.0</li>
                </ol>
            </div><!--//container-->
        </header><!--//header-->
        <div class="doc-wrapper">
            <div class="container">
                <div id="doc-header" class="doc-header text-center">
                    <h1 class="doc-title"><span aria-hidden="true" class="icon icon_datareport_alt"></span> API V1.0</h1>
                    <div class="meta"><i class="fa fa-clock-o"></i> Last updated: April 19th, 2017</div>
                </div><!--//doc-header-->
                <div class="doc-body">
                    <div class="doc-content">
                        <div class="content-inner">
                            <section id="outlets" class="doc-section">
                                <h2 class="section-title">GET ACTIVE OUTLETS</h2>
                                <div class="section-block">
                                    <p>Returns all of the active outlets. The request to be made is GET on the URL: <?php echo base_url();?>api/getOutlets</p>
                                    <div class="code-block">
                                        <code class="language-javascript" style="padding: 10px;">GET <?php echo base_url();?>api/getOutlets</code>
                                    </div>
                                    <p>Array which is json encoded looks like below:</p>
                                    <div class="screenshot-holder">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets_documentation/images/Outlet.PNG" alt="screenshot" />                                        
                                    </div> 
                                </div><!--//section-block-->
                                <!--//section-block-->
                            </section><!--//doc-section-->
                            
                            <section id="sliders" class="doc-section">
                                <h2 class="section-title">GET ACTIVE SLIDERS</h2>
                                <div class="section-block">
                                    <p>Returns all of the active sliders. The request to be made is GET on the URL: <?php echo base_url();?>api/getSliders</p>
                                    <div class="code-block">
                                        <code class="language-javascript" style="padding: 10px;">GET <?php echo base_url();?>api/getSliders</code>
                                    </div>
                                    <p>Array which is json encoded looks like below:</p>
                                    <div class="screenshot-holder">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets_documentation/images/Slider.PNG" alt="screenshot" />                                        
                                    </div> 
                                </div><!--//section-block-->
                                <!--//section-block-->
                            </section>
                            
                            <section id="offers" class="doc-section">
                                <h2 class="section-title">GET ACTIVE OFFERS</h2>
                                <div class="section-block">
                                    <p>Returns all of the active offers. The request to be made is GET on the URL: <?php echo base_url();?>api/getOffers</p>
                                    <div class="code-block">
                                        <code class="language-javascript" style="padding: 10px;">GET <?php echo base_url();?>api/getOffers</code>
                                    </div>
                                    <p>Array which is json encoded looks like below:</p>
                                    <div class="screenshot-holder">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets_documentation/images/Offer.PNG" alt="screenshot" />                                        
                                    </div> 
                                </div><!--//section-block-->
                                <!--//section-block-->
                            </section>
                            
                            <section id="cable" class="doc-section">
                                <h2 class="section-title">GET ACTIVE CABLE OPERATORS</h2>
                                <div class="section-block">
                                    <p>Returns all of the active cable operators. The request to be made is GET on the URL: <?php echo base_url();?>api/getCableOperators</p>
                                    <div class="code-block">
                                        <code class="language-javascript" style="padding: 10px;">GET <?php echo base_url();?>api/getCableOperators</code>
                                    </div>
                                    <p>Array which is json encoded looks like below:</p>
                                    <div class="screenshot-holder">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets_documentation/images/Operator.PNG" alt="screenshot" />                                        
                                    </div> 
                                </div><!--//section-block-->
                                <!--//section-block-->
                            </section>
                            
                            <section id="packages" class="doc-section">
                                <h2 class="section-title">GET ACTIVE PACKAGES</h2>
                                <div class="section-block">
                                    <p>Returns all of the active packages. The request to be made is GET on the URL: <?php echo base_url();?>api/getPackages</p>
                                    <div class="code-block">
                                        <code class="language-javascript" style="padding: 10px;">GET <?php echo base_url();?>api/getPackages</code>
                                    </div>
                                    <p>Array which is json encoded looks like below:</p>
                                    <div class="screenshot-holder">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets_documentation/images/Packages.png" alt="screenshot" />                                        
                                    </div> 
                                </div><!--//section-block-->
                                <!--//section-block-->
                            </section>
                            
                            <section id="pages" class="doc-section">
                                <h2 class="section-title">GET ACTIVE Pages</h2>
                                <div class="section-block">
                                    <p>Returns all of the active pages along with its categories. So we dont need seperate API for page categories. Since, Category title is also merged with the page, so the array structure is different than usual. The request to be made is GET on the URL: <?php echo base_url();?>api/getPages</p>
                                    <div class="code-block">
                                        <code class="language-javascript" style="padding: 10px;">GET <?php echo base_url();?>api/getPages</code>
                                    </div>
                                    <p>Array which is json encoded looks like below:</p>
                                    <div class="screenshot-holder">
                                        <img class="img-responsive" src="<?php echo base_url();?>assets_documentation/images/Page.PNG" alt="screenshot" />                                        
                                    </div> 
                                </div><!--//section-block-->
                                <!--//section-block-->
                            </section>
                            
                            

                            
                        </div><!--//content-inner-->
                    </div><!--//doc-content-->
                    <div class="doc-sidebar">
                        <nav id="doc-nav">
                            <ul id="doc-menu" class="nav doc-menu hidden-xs" data-spy="affix">
                                <li><a class="scrollto" href="#outlets">Outlets</a></li>                                
                                <li><a class="scrollto" href="#sliders">Sliders</a></li>
                                <li><a class="scrollto" href="#offers">Offers</a></li>
                                <li><a class="scrollto" href="#cable">Cable Operators</a></li>
                                <li><a class="scrollto" href="#packages">Packages</a></li>
                                <li><a class="scrollto" href="#pages">Pages</a></li>
                            </ul><!--//doc-menu-->
                        </nav>
                    </div><!--//doc-sidebar-->
                </div><!--//doc-body-->              
            </div><!--//container-->
        </div><!--//doc-wrapper-->
        
        <!--//promo-block-->
        
    </div><!--//page-wrapper-->
    
    <footer id="footer" class="footer text-center">
        <div class="container">
            <!--/* This template is released under the Creative Commons Attribution 3.0 License. Please keep the attribution link below when using for your own project. Thank you for your support. :) If you'd like to use the template without the attribution, you can check out other license options via our website: themes.3rdwavemedia.com */-->
            <small class="copyright">&copy;2017 | Powered by Shiran Technologies</small>
            
        </div><!--//container-->
    </footer><!--//footer-->
    
     
    <!-- Main Javascript -->          
    <script type="text/javascript" src="<?php echo base_url();?>assets_documentation/plugins/jquery-1.12.3.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets_documentation/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets_documentation/plugins/prism/prism.js"></script>    
    <script type="text/javascript" src="<?php echo base_url();?>assets_documentation/plugins/jquery-scrollTo/jquery.scrollTo.min.js"></script>  
    <script type="text/javascript" src="<?php echo base_url();?>assets_documentation/plugins/lightbox/dist/ekko-lightbox.min.js"></script>                                                                
    <script type="text/javascript" src="<?php echo base_url();?>assets_documentation/plugins/jquery-match-height/jquery.matchHeight-min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets_documentation/js/main.js"></script>
    
</body>
</html> 

