<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardM extends CI_Model {

	function getSetting()
    {
        return $this->db->get('tbl_settings')->row();        
    }
    public function saveSetting($id,$fields)
	{
		foreach($this->input->post() as $k=>$v)
        {
            if(in_array($k,$fields))
            {
                if($k=='site_logo' && $v=='')
                {
                 continue; 
                }
                else
                {
                        if($k=='site_logo')
                        $this->session->set_userdata('site_logo',$v);
                }
                $arr[$k] = trim($v);
            }
        }
        $this->session->set_userdata('site_color',$arr['site_color']);
        $this->session->set_userdata('site_title',$arr['site_title']);
        if(!$id)
        {
            $str = 'added';
            if($arr){
                $this->db->insert('tbl_settings', $arr); 
                }

        }
        else
        {
            $str = 'updated';
            if($arr){
                $this->db->where('id',$id);
                $this->db->update('tbl_settings', $arr);
            } 
        }
        return $str;
	}
    
}