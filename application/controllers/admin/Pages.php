<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
    public function __construct()
    {
        
        parent::__construct();
        if(!$this->session->userdata('id'))
        {
            redirect('login');
        }
        $this->load->library('pagination');
        $this->load->library('Common');
        $this->load->model('Page');
    } 
	public function index($offset=0)
	{
        $data['offset'] = $offset;
        $config = $this->common->getPaginationConfig(base_url().'admin/pages/index/');
        $config['total_rows'] = $this->Page->countAll();
        $data['pages'] = $this->Page->listAllPages($config['per_page'],$offset);
        $this->pagination->initialize($config);
	    $data['title'] = 'Pages - '.$this->session->userdata('site_title');
		$this->load->view('admin/include/header',$data);
        $this->load->view('admin/pages/index',$data);
        $this->load->view('admin/include/footer');
	}
    
    public function add($id=0)
    {
        $this->load->library('Common');
        $fields = $this->common->fields('tbl_pages');
        if($id == 0)
        {
            $page = $this->common->setBlankModel($fields);
        }
        else
        {
            $page = $this->Page->getByAttributes(array('id'=>$id));
        }
        $data['categories'] = $this->Page->getAllByAttributes(array('parent_id'=>0));
        if($this->input->post())
        {
            $status = $this->Page->saveData($id,$fields);
            $this->session->set_flashdata('message','Page '.$status.' successfully');
            redirect('admin/pages');
        }
        $data['model'] = $page;
        $data['title'] = 'Add/Edit Page - '.$this->session->userdata('site_title');
        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/pages/add',$data);
        $this->load->view('admin/include/footer');
    }
    
    public function delete($id)
    {
        $this->Page->deleteByAttr(array('id'=>$id));
        $this->session->set_flashdata('message','Page has been deleted successfully');
        redirect('admin/pages');
    }
    
}
