
<!--------------BANNER---------->
<section class="banner-body">
  <div class="container banner-main">
      <div class="slider-container">
  <div class="slider">
    <?php
        if($sliders)
        {
            foreach($sliders as $s)
            {
            ?>
                <div class="slider__item">
                    <img src="<?php echo $s['image'];?>" alt="">
                    <div class="slider__caption"><p><?php echo $s['caption']; ?>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p></div>
                </div>
            <?php
            }
        }
    ?>
  </div>
  <div class="slider__switch slider__switch--prev" data-ikslider-dir="prev">
    <span><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 20 20"><path d="M13.89 17.418c.27.272.27.71 0 .98s-.7.27-.968 0l-7.83-7.91c-.268-.27-.268-.706 0-.978l7.83-7.908c.268-.27.7-.27.97 0s.267.71 0 .98L6.75 10l7.14 7.418z"/></svg></span>
  </div>
  <div class="slider__switch slider__switch--next" data-ikslider-dir="next">
    <span><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 20 20"><path d="M13.25 10L6.11 2.58c-.27-.27-.27-.707 0-.98.267-.27.7-.27.968 0l7.83 7.91c.268.27.268.708 0 .978l-7.83 7.908c-.268.27-.7.27-.97 0s-.267-.707 0-.98L13.25 10z"/></svg></span>
  </div>
</div>
    <!-- End Carousel -->
</div>
</section>

<!--------------COMPANY PROFILE------------>
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 border">
            <div class="media ">
            <h3>About Us<span class="h3-border"></span></h3>
                <div class="media-left media-top description-logo-wrapper"><div class="description-logo"><img  src="<?php echo site_url('assets/images/logo.png');?>"></div></div>
                <?php
                    if($aboutus)
                    {
                        ?>
                        <div class="media-body">
                            <p class="company-profile-info"><?php echo word_limiter($aboutus->description , 80, '')."....";?></p>
                       
                </div>
                 <div class="btn-wrapper"><a href="<?php echo site_url('pages/details/'.$aboutus->slug); ?>" class="center-block text-center btn-style">VIEW MORE</a></div>
                        <?php
                    }
                ?>
                
            </div>
      </div>      
    </div>    
    </div>
</section>
<!--------------NEWS AND ASIDE------------>
<section>
  <div class="news-quicklink-main">
      <div class="container">
          <div class="row">
              <div class="col-lg-9 news-body">
                  <div class="main-heading"><h3>NEWS</h3></div>
                      <div class="row news-main">
                      <?php
                        if($news)
                        {
                          foreach($news as $n)
                          {
                            ?>
                            <div class="col-lg-4 col-md-6 news-main-list">
                              <div class="news-main-list-img">
                              <a href="<?php echo site_url('news/details/'.$n->slug); ?>" style="display: block;">
                              <img src="<?php echo site_url('assets/images/news/resized/'.$n->image); ?>"></div>
                                <span class="date"><?php echo $time_elapsed = $mTime->timeElapsed(strtotime($n->publish_date))."&nbsp;"."ago";?></span>
                                <p><?php echo $n->description."..."; ?></p>
                                </a>
                            </div>
                            <?php
                          }
                        }
                      ?>
                            
                            
                        </div>
                        <div class="btn-wrapper"><a href="<?php echo site_url('news'); ?>" class="center-block text-center btn-style">VIEW MORE</a></div>
                    </div>
                <?php $this->load->view('common/sidebar');?>
            </div>
        </div>
    </div>
</section>
