<script src="<?php echo site_url('assets/js/upload.js');?>"></script>
<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">build</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Theme Settings</h4>
                                    <div class="gap"></div>
                                    <form action="" method="post">
                                        <div class="col-md-12">
                                            <div class="form-group label-floating nomar">
                                                <label class="control-label">Site Title</label>
                                                <input type="text" name="site_title" value="<?php if($model)echo $model->site_title;?>" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="gap"></div>
                                        
                                        <div class="col-md-3">
                                            <div class="form-group label-floating nomar">
                                                <label>Site Logo</label><br />
                                                <a href="javascript:void(0);" id="upload" class="btn btn-primary">Browse</a>
                                                <input type="hidden" name="site_logo" class="site_logo" />
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="img-container" style="border: 2px solid #CCC;background:#f5f5f5;width:200px;<?php if($model && $model->site_logo){?>height:auto;<?php }else{?>height:200px;<?php }?>">
                                                <?php if($model && $model->site_logo){?><img src="<?php echo site_url('assets/img/faces/'.$model->site_logo)?>" style="max-width: 100%;" /><?php }?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="gap"></div>
                                        
                                        <div class="col-md-12">
                                            <div class="form-group label-floating nomar">
                                                <label>Theme Color</label><br />
                                                <?php
                                                $colors = array('white','black','red','green','blue');
                                                foreach($colors as $c)
                                                {
                                                    ?>
                                                    <input type="radio" name="site_color" <?php if($model && $model->site_color==$c){?>checked<?php }?> value="<?php echo $c;?>" /> <?php echo ucfirst($c)?> &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <?php
                                                }
                                                ?>
                                                  
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="gap"></div>
                                            
                                        
                                        
                                        
                                        
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-rose pull-right">Save Theme</button>
                                        </div>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
            <script type="text/javascript">
            $(function(){
                initiate_ajax_upload1('upload');
            })
                function initiate_ajax_upload1(button_id) {
                //alert(button_id);
                var button = $('#' + button_id), interval;
                
                new AjaxUpload(button, {
                    action: '<?php echo site_url('admin/dashboard/upload')?>',
                    name: 'file',
                    onSubmit: function (file, ext) {
                        button.text('Uploading');
                        this.disable();
                        interval = window.setInterval(function () {
                            var text = button.text();
                            if (text.length < 13) {
                                button.text(text + '.');
                            } else {
                                button.text('Uploading');
                            }
                        }, 200);
                    },
                    onComplete: function (file, response) {
                            this.enable();
                            button.html('Browse');
                            var obj = JSON.parse(response);
                            if(obj.error)
                            {
                                alert(obj.error);
                            }
                            else
                            {
                                $('.site_logo').val(obj.file);
                                $('.img-container').html('<img src="<?php echo base_url().'assets/img/faces/';?>'+obj.file+'" style="max-width:100%;" />');
                                
                            }
                            window.clearInterval(interval);
        
                        
                    }
                });
            }
            </script>