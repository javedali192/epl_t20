<script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script src="<?php echo site_url('assets/js/ckfinder/ckfinder_test.js');?>"></script>
<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="green">
                                    <i class="material-icons">description</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Add/Edit Page</h4>
                                    
                                        <div class="col-md-10">
                                            <div class="table-responsive table-sales">
                                                    <form action="" method="post" id="settings">
                                                            <div class="col-md-12">                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label>Parent Page</label>
                                                                        <select class="form-control" name="parent_id">
                                                                            <option value="">Select Parent Page</option>
                                                                            <?php
                                                                            foreach($categories as $c)
                                                                            {
                                                                                ?>
                                                                                    <option value="<?php echo $c->id?>" <?php if($model->parent_id == $c->id){?>selected<?php }?>><?php echo $c->title;?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                
                                                            </div> 
                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Title</label>
                                                                        <input type="text" class="form-control" required="" name="title" value="<?php echo $model->title;?>">
                                                                    </div>
                                                                
                                                            </div>                                                            
                                                            <div class="clearfix"></div>
                                                                                                                        
                                                            <div class="col-md-12">
                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label>Description</label>
                                                                        <textarea name="description" required=""><?php echo $model->description;?></textarea>
                                                                        
                                                                    </div>
                                                                
                                                            </div>                                                            
                                                            <div class="clearfix"></div>
                                                            
                                                            
                                                            
                                                            <hr />
                                                            
                                                            <div class="col-md-12">                                                                
                                                                    <div class="form-group label-floating">
                                                                        <label class="control-label">Is active?</label>
                                                                        <input type="radio" name="is_active" value="1" <?php if($model->is_active){?>checked="checked"<?php }?> /> Yes &nbsp; &nbsp; <input type="radio" name="is_active" value="0" <?php if(!$model->is_active){?>checked="checked"<?php }?> /> No  
                                                                    </div>                                                                
                                                            </div>                                                            
                                                            <div class="clearfix"></div>
                                                            
                                                            <hr />
                                                            <div class="gap"></div>
                                                            
                                                            <div class="col-md-12">
                                                                <button type="submit" class="btn btn-rose pull-right"><?php if($model->id){?>Update<?php }else{?>Create<?php }?></button>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        
                                                        
                                                    </form>
                                            </div>
                                        </div>
                                        
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>
            <style>
            .error{color:#CD0909!important;}
            </style>
            <script>
            
            $(function(){
                var editor = CKEDITOR.replace( 'description' );
                CKFinder.setupCKEditor( editor );
                

            })
            
            </script>
            