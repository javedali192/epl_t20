<!--------------NEWS AND ASIDE------------>
<section class="section_wrapper">
  <div class="news-quicklink-main">
      <div class="container">
          <div class="row">
              <div class="col-lg-9 news-body">
                  <div class="main-heading"><h3>NEWS</h3></div>
                      <div class="row news-main">
                      <?php
                        if($news)
                        {
                          foreach($news as $n)
                          {
                            ?>
                            
                            <div class="col-lg-6 news-main-list">
                              <div class=" news-page-list">
                              <a href="<?php echo site_url('news/details/'.$n->slug); ?>" style="display: block;">
                              <img src="<?php echo site_url('assets/images/news/resized/'.$n->image); ?>"></div>
                                <span class="date"><?php echo $time_elapsed = $mTime->timeElapsed(strtotime($n->publish_date))."&nbsp;"."ago";?></span>
                                <p><?php echo $n->description."..."; ?></p>
                                </a>
                            </div>
                          
                            <?php
                          }
                        }
                      ?>
                            
                            
                        </div>
                        <div class="pagination">
                           <?php echo $this->pagination->create_links();?>  
                        </div>
                    </div>
                <?php $this->load->view('common/sidebar');?>
            </div>
        </div>
    </div>
</section>