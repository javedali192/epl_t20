<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../../assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Admin - Login</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <link href="<?php echo site_url('assets/css/bootstrap.min.css');?>" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="<?php echo site_url('assets/css/material-dashboard.css');?>" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="<?php echo site_url('assets/css/demo.css');?>" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="<?php echo site_url('assets/css/fa/css/font-awesome.min.css')?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
</head>

<body>
    
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="black" data-image="../../assets/img/login.jpeg">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                            <form method="post" action="<?php echo site_url('admin/login/verify');?>">
                                
                                <div class="card card-login card-hidden">
                                    <div class="card-header text-center" data-background-color="rose">
                                        <h4 class="card-title">Login</h4>
                                        
                                    </div>
                                    
                                    <div class="card-content">
                                        <p class="login-error"><?php echo $this->session->flashdata('error');?></p>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">email</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Email address / Username</label>
                                                <input type="text" required="" name="username" class="form-control">
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Password</label>
                                                <input type="password" required="" name="password" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer text-center">
                                        <button type="submit" class="btn btn-rose btn-simple btn-wd btn-lg">Login</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</body>

<!--   Core JS Files   -->
<script src="<?php echo site_url('assets/js/jquery-3.1.1.min.js');?>" type="text/javascript"></script>
<script src="<?php echo site_url('assets/js/jquery-ui.min.js');?>" type="text/javascript"></script>
<script src="<?php echo site_url('assets/js/bootstrap.min.js');?>" type="text/javascript"></script>
<script src="<?php echo site_url('assets/js/material.min.js');?>" type="text/javascript"></script>
<script src="<?php echo site_url('assets/js/perfect-scrollbar.jquery.min.js');?>" type="text/javascript"></script>
<!-- Forms Validations Plugin -->
<script src="<?php echo site_url('assets/js/jquery.validate.min.js');?>"></script>
<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="<?php echo site_url('assets/js/moment.min.js');?>"></script>
<!--  Charts Plugin -->
<script src="<?php echo site_url('assets/js/chartist.min.js');?>"></script>
<!--  Plugin for the Wizard -->
<script src="<?php echo site_url('assets/js/jquery.bootstrap-wizard.js');?>"></script>
<!--  Notifications Plugin    -->
<script src="<?php echo site_url('assets/js/bootstrap-notify.js');?>"></script>
<!--   Sharrre Library    -->
<script src="<?php echo site_url('assets/js/jquery.sharrre.js');?>"></script>
<!-- DateTimePicker Plugin -->
<script src="<?php echo site_url('assets/js/bootstrap-datetimepicker.js');?>"></script>
<!-- Vector Map plugin -->
<script src="<?php echo site_url('assets/js/jquery-jvectormap.js');?>"></script>
<!-- Sliders Plugin -->
<script src="<?php echo site_url('assets/js/nouislider.min.js');?>"></script>

<!-- Select Plugin -->
<script src="<?php echo site_url('assets/js/jquery.select-bootstrap.js');?>"></script>
<!--  DataTables.net Plugin    -->
<script src="<?php echo site_url('assets/js/jquery.datatables.js');?>"></script>
<!-- Sweet Alert 2 plugin -->
<script src="<?php echo site_url('assets/js/sweetalert2.js');?>"></script>
<!--	Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
<script src="<?php echo site_url('assets/js/jasny-bootstrap.min.js');?>"></script>
<!--  Full Calendar Plugin    -->
<script src="<?php echo site_url('assets/js/fullcalendar.min.js');?>"></script>
<!-- TagsInput Plugin -->
<script src="<?php echo site_url('assets/js/jquery.tagsinput.js');?>"></script>
<!-- Material Dashboard javascript methods -->
<script src="<?php echo site_url('assets/js/material-dashboard.js');?>"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="<?php echo site_url('assets/js/demo.js');?>"></script>
<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>

</html>