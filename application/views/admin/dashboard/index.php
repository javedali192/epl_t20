<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <a href="<?php echo site_url('admin/pages');?>">
                                    <div class="card-header" data-background-color="orange">
                                        <i class="material-icons">description</i>
                                    </div>
                                    <div class="card-content">
                                        <p class="category">Pages</p>
                                        <h3 class="card-title"><?php echo $count_pages;?></h3>
                                    </div>
                                </a>
                            </div>
                        </div>
                        
                        
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <a href="<?php echo site_url('admin/sliders');?>">
                                    <div class="card-header" data-background-color="blue">
                                        <i class="material-icons">code</i>
                                    </div>
                                    <div class="card-content">
                                        <p class="category">Sliders</p>
                                        <h3 class="card-title"><?php echo $count_sliders;?></h3>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3>Welcome</h3>
                                </div>
                                <div class="card-content">
                                    <p>You are logged in as : <strong><?php echo $this->session->username;?></strong></p>
                                    <p>Role :  <strong><?php 
                                    $arr['1'] = 'Super Admin';
                                    $arr['2'] = 'Editor';
                                    echo $arr[$this->session->role];?></strong></p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>